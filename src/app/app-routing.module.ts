import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CucmuserComponent } from './components/cucmuser/cucmuser.component';
import { CucmuserEditComponent } from './components/cucmuser-edit/cucmuser-edit.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserComponent } from './components/user/user.component';



const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'cucmusers', component: CucmuserComponent},
  { path: 'cucmusers/edit/:id', component: CucmuserEditComponent},
  { path: 'cucmusers/edit', component: CucmuserEditComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register/:username', component: RegisterComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'users', component: UserComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
