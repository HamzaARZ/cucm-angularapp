import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { throwError} from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LoginRequest } from 'src/app/modeles/LoginRequest';
import { RegisterRequest } from 'src/app/modeles/RegisterRequest';
import { UserService } from './user.service';

// export const HTTP_OPTIONS = {
//   headers: new HttpHeaders({
//     'Content-Type':  'application/json',
//     'Access-Control-Allow-Credentials' : '*',
//     'Access-Control-Allow-Origin': '*',
//     'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
//     'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
//   })
// };

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public token;

  constructor(private http : HttpClient) { }

  login(loginRequest: LoginRequest): Observable<any> {
    return this.http.post('http://localhost:8080/auth/login', loginRequest);
  }

  register(registerRequest: RegisterRequest): Observable<any>{
    return this.http.post('http://localhost:8080/users', registerRequest)
      .pipe(catchError(this.errorHandler));
  }

  updateUser(username: String, registerRequest: RegisterRequest): Observable<any>{
    let headers = this.getTokenHeader();
    return this.http.put('http://localhost:8080/users' + "/" + username, registerRequest, {headers: headers})
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error : HttpErrorResponse){
    return throwError(error || "server error !!");
  }

  getTokenHeader(): HttpHeaders{
    this.token = "Bearer " + this.getToken();
    console.log(this.token);
    let authHeader = {'Authorization': this.token};
    let headers: HttpHeaders = new HttpHeaders(authHeader);
    return headers;
    // let token = "";
    // let authHeader = {'Authorization': token};
    // let headers: HttpHeaders = new HttpHeaders(authHeader);
  }


  hasPermission(permission: string): Boolean{
    if(this.getPermissions() === null){
      return false;
    }
    return this.getPermissions().includes(permission);
  }

  setToken(token){
    localStorage.setItem('token', token);
  }
  getToken(){
    return localStorage.getItem('token');
  }
  removeToken(){
    localStorage.removeItem('token');
  }

  isLoggedIn(): Boolean{
    return localStorage.getItem('token') !== null;
  }

  logout(){
    this.removeToken();
    this.removeUsername();
    this.removePermissions();
  }

  setUsername(username){
    localStorage.setItem('username', username);
  }
  getUsername(){
    return localStorage.getItem('username');
  }
  removeUsername(){
    localStorage.removeItem('username');
  }
  
  setPermissions(permissions){
    localStorage.setItem('permissions', permissions);
  }
  getPermissions(){
    return localStorage.getItem('permissions');
  }

  removePermissions(){
    localStorage.removeItem('permissions');
  }
}
