import { TestBed } from '@angular/core/testing';

import { CucmuserService } from './cucmuser.service';

describe('CucmuserService', () => {
  let service: CucmuserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CucmuserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
