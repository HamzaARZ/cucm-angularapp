import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CucmuserEditComponent } from './cucmuser-edit.component';

describe('CucmuserEditComponent', () => {
  let component: CucmuserEditComponent;
  let fixture: ComponentFixture<CucmuserEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CucmuserEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CucmuserEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
